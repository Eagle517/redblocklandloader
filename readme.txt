RedBlocklandLoader

Automatically loads DLL mods for Blockland.
Like BlocklandLoader, but for Blockland r2005+.
Currently tested working with r2033.

Installation and use is exactly the same as BlocklandLoader.

How to Install:
1. From the Releases page, place RedBlocklandLoader.dll in the same folder as Blockland.exe
2. from the Releases page, copy Blockland.exe over your existing Blockland.exe (in Documents/Blockland)
3. Make Blockland.exe read-only (rightclick -> properties -> attributes -> Read-only) so the launcher cannot overwrite it.
4. Create a folder called modules in the same folder as Blockland.exe. This is where you will place DLL mods.
If all goes well, you should see "RedBlocklandLoader Init" in console near the beginning of loading.

How to Install, for pros:
1. Generate build configuration using CMake. The project is only tested using MinGW, but may work with other compilers.
2. Place RedBlocklandLoader.dll in the same folder as Blockland.exe
3. Use StudPE or similar PE editor to add an import from the DLL to Blockland.exe. This will be the same process as for the old BlocklandLoader.
4. Create a folder called modules in the same folder as Blockland.exe. This is where you will place DLL mods.
If you get an error saying "The application failed to start" or similar, it is probably because you are using the wrong verison of the game, and RedBlocklandLoader could not find the InitGame function signature.
Make sure you are using a supported version of Blockland.
